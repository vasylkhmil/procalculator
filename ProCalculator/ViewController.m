//
//  ViewController.m
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/2/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import "ViewController.h"
#import "Event.h"
#import "ViewOperationOfCalculatorCell.h"
#import "AppDelegate.h"

@implementation ViewController

@synthesize listOfOperations = _listOfOperations;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStyleBordered target:self action:@selector(deleteCells:)];
    [self.navigationItem setRightBarButtonItem:addButton];
    [super viewDidLoad];
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(addTime:)];
//	self.navigationItem.rightBarButtonItem = addButton;
//	[addButton release];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (IBAction) deleteCells:(id)pSender{
    if(self.editing)
	{
		[self setEditing:NO animated:NO];
		[self.navigationItem.rightBarButtonItem setTitle:@"delete"];
		//[self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
	}
	else
	{
		[self setEditing:YES animated:YES];
		[self.navigationItem.rightBarButtonItem setTitle:@"Done"];
		//[self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_listOfOperations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ViewOperationOfCalculatorCell";
    ViewOperationOfCalculatorCell *cell = (ViewOperationOfCalculatorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ViewOperationOfCalculatorCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
//    ViewOperationOfCalculatorCell *cell = (ViewOperationOfCalculatorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[[ViewOperationOfCalculatorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//    }
    
    cell.dateLabel.text = [(NSDate *)[(Event *)[_listOfOperations objectAtIndex:indexPath.row] time] description];
    cell.operationLabel.text = [((Event *)[_listOfOperations objectAtIndex:indexPath.row]) str];
    cell.resultLabel.text = [((Event *)[_listOfOperations objectAtIndex:indexPath.row]) result];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
     */
    // Remove the row from data model
    NSManagedObjectContext *managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:managedObjectContext];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entity];
//    //[[(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext] deleteObject:lObjectForDeleting];
//    NSError *error;
//    NSArray *objectsOfDataBase = [managedObjectContext executeFetchRequest:request error:&error];
//    [managedObjectContext deleteObject:[objectsOfDataBase objectAtIndex:indexPath.row]];
    [managedObjectContext deleteObject:[_listOfOperations objectAtIndex:indexPath.row]];
    [managedObjectContext save:NULL];
    NSLog(@"%@", _listOfOperations);
    //[_listOfOperations removeObjectAtIndex:indexPath.row];
    
    [tableView reloadData];
}
 
 


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
