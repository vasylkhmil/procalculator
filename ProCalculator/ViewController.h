//
//  ViewController.h
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/2/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UITableViewController{
    NSMutableArray *_listOfOperations;
}

@property (nonatomic, retain) NSArray *listOfOperations;

@end

