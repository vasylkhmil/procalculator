//
//  Event.m
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/2/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic result;
@dynamic time;
@dynamic str;

@end
