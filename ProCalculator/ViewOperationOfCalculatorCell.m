//
//  ViewOperationOfCalculatorCell.m
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/3/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import "ViewOperationOfCalculatorCell.h"

@implementation ViewOperationOfCalculatorCell

@synthesize dateLabel = _dateLabel;
@synthesize operationLabel = _operationLabel;
@synthesize resultLabel = _resultLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
