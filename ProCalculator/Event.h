//
//  Event.h
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/2/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * result;
@property (nonatomic, retain) NSString * str;
@property (nonatomic, retain) NSDate * time;

@end
