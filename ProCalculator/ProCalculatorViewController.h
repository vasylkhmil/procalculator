//
//  ProCalculatorViewController.h
//  ProCalculator
//
//  Created by Khmil Vasyl on 6/26/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface ProCalculatorViewController : UIViewController{
    UITextField *mString;
    NSManagedObjectContext *_managedObjectContext;
}

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end
