//
//  ProCalculatorViewController.m
//  ProCalculator
//
//  Created by Khmil Vasyl on 6/26/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import "ProCalculatorViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"

@implementation ProCalculatorViewController

@synthesize managedObjectContext = _managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.managedObjectContext = [(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor redColor]];
    mString = [[UITextField alloc] initWithFrame:CGRectMake(5, 20, 310, 25)];
    [mString setBackgroundColor:[UIColor blackColor]];
    [mString setTextColor:[UIColor whiteColor]];
    [mString setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [self.view addSubview:mString];
    [mString addTarget:self action:@selector(stringValueChanged:) forControlEvents:UIControlEventEditingChanged ];
    [mString release];
    UIButton *lResult = [[UIButton alloc] initWithFrame:CGRectMake(100, 50, 100, 50)];
    [lResult addTarget:self action:@selector(buttonResultPressed:) forControlEvents:UIControlEventTouchUpInside ];
    [lResult setTitle:@"Result" forState:UIControlStateNormal];
    [lResult setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:lResult];
    UIButton *lMemory = [[UIButton alloc] initWithFrame:CGRectMake(210, 50, 100, 50)];
    [lMemory addTarget:self action:@selector(buttonMemoryPressed:) forControlEvents:UIControlEventTouchUpInside ];
    [lMemory setTitle:@"Memory" forState:UIControlStateNormal];
    [lMemory setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:lMemory];
    [lMemory release];
}

- (void)buttonMemoryPressed:(id)pSender{
    ViewController *lViewController = [[ViewController alloc] init];
    lViewController.listOfOperations = [self readDataBase];
    [self.navigationController pushViewController:lViewController animated:YES];
    //NSLog(@"%@", [((NSDictionary *)[[self readDataBase] objectAtIndex:0]) valueForKey:@"str"]);
}

- (NSArray *)readDataBase{
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES];
//    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:_managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entity];
    //[request setSortDescriptors:sortDescriptors];
    NSArray *lResult = [_managedObjectContext executeFetchRequest:request error:NULL];
    return lResult;
}

- (void)buttonResultPressed:(id)pSender{
    if ([self stringCanBeCalculated:mString.text]){
        NSString *lResult = [self calculateString:mString.text];
        UILabel *lRes = [[UILabel alloc] initWithFrame:CGRectMake(5, 70, 80, 25)];
        [self.view addSubview:lRes];
        lRes.text = lResult;
        //mString.text = lResult;
        //[(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:_managedObjectContext];
        NSManagedObject *lObject = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:_managedObjectContext];
        [lObject setValue:[NSDate date] forKey:@"time"];
        [lObject setValue:lResult forKey:@"result"];
        [lObject setValue:mString.text forKey:@"str"];
        NSError *error = nil;
        [_managedObjectContext save:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    }
}

- (BOOL)stringCanBeCalculated:(NSString *)lStr{
    return YES;
}

- (void)stringValueChanged:(id)pSender{
    
}

- (NSString *)calculateString:(NSString *)lStr{
    NSString *lNewStr = [[NSString alloc] initWithString:lStr];
    while ([lNewStr rangeOfString:@"("].location != NSNotFound){
        NSInteger lIndexOfLast = [lNewStr rangeOfString:@"("].location;
        NSInteger  lCount = 1;
        NSInteger lIndexOfFirst = [lNewStr rangeOfString:@"("].location;
        while ([lNewStr characterAtIndex:lIndexOfLast + 1] != ')'){
            if ([lNewStr characterAtIndex:lIndexOfLast + 1] == '('){
                ++lCount;
            }
            ++lIndexOfLast;
        }
        NSInteger lSecontCount = 0;
        while (lSecontCount != lCount) {
            if ([lNewStr characterAtIndex:lIndexOfLast + 1] == ')') {
                ++lSecontCount;
            }
            ++lIndexOfLast;
        }
        NSString *lValueInside = [[NSString alloc] initWithString: [lNewStr substringWithRange:NSMakeRange(lIndexOfFirst + 1, lIndexOfLast - lIndexOfFirst - 1)]];
        lNewStr = [lNewStr stringByReplacingCharactersInRange:NSMakeRange(lIndexOfFirst, lIndexOfLast - lIndexOfFirst) withString:[self calculateString:lValueInside]];
        [lValueInside release];
    }
    lNewStr = [NSString stringWithString:[self calculateElementaryString:lNewStr]];
    return lNewStr;
}

- (NSString *)calculateElementaryString:(NSString *)lStr{
    NSArray *lArr_1 = [lStr componentsSeparatedByString:@"-"];
    NSMutableArray *lMinuseArray = [[NSMutableArray alloc] initWithCapacity:10];
    [lMinuseArray setArray:lArr_1];
    float lMinuseResult;
    for (NSInteger i = 0; i < [lMinuseArray count]; ++i){
        NSArray *lArr_2 = [[lMinuseArray objectAtIndex: i] componentsSeparatedByString:@"+"];
        NSMutableArray *lPlussArray = [[NSMutableArray alloc] initWithCapacity:10];
        [lPlussArray setArray:lArr_2];
        float lPlussResult = 0;
        for (NSInteger j = 0; j < [lPlussArray count]; ++j){
            NSArray *lArr_3 = [[lPlussArray objectAtIndex: j] componentsSeparatedByString:@"/"];
            NSMutableArray *lDivideArray = [[NSMutableArray alloc] initWithCapacity:10];
            [lDivideArray setArray:lArr_3];
            float lDivideResult;
            for (NSInteger m = 0; m < [lDivideArray count]; ++m){
                NSArray *lArr_4 = [[lDivideArray objectAtIndex:m] componentsSeparatedByString:@"*"];
                NSMutableArray *lSubArray = [[NSMutableArray alloc] initWithCapacity:10];
                [lSubArray setArray:lArr_4];
                float lSubResult = 1;
                for (NSInteger l = 0; l < [lSubArray count]; ++l){
                    lSubResult *= [[lSubArray objectAtIndex:l] floatValue];
                }
                [lDivideArray replaceObjectAtIndex:m withObject:[NSString  stringWithFormat:@"%f", lSubResult]];
                if (m == 0){
                    lDivideResult = [[lDivideArray objectAtIndex:m] floatValue];
                } else{
                    lDivideResult /= [[lDivideArray objectAtIndex:m] floatValue];
                }
                [lSubArray release];
            }
            [lPlussArray replaceObjectAtIndex:j withObject:[NSString  stringWithFormat:@"%f", lDivideResult]];
            lPlussResult += [[lPlussArray objectAtIndex:j] floatValue];
            [lDivideArray release];
        }
        [lMinuseArray replaceObjectAtIndex:i withObject:[NSString  stringWithFormat:@"%f", lPlussResult]];
        if (i == 0){
            lMinuseResult = [[lMinuseArray objectAtIndex:i] floatValue];
        } else{
            lMinuseResult -= [[lMinuseArray objectAtIndex:i] floatValue];
        }
        [lPlussArray release];
    }
    [lMinuseArray release];
    return [NSString stringWithFormat:@"%2.f", lMinuseResult];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
