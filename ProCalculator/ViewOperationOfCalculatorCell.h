//
//  ViewOperationOfCalculatorCell.h
//  ProCalculator
//
//  Created by Khmil Vasyl on 7/3/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewOperationOfCalculatorCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *dateLabel;
@property (nonatomic, retain) IBOutlet UILabel *operationLabel;
@property (nonatomic, retain) IBOutlet UILabel *resultLabel;

@end
