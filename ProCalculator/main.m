//
//  main.m
//  ProCalculator
//
//  Created by Khmil Vasyl on 6/26/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
