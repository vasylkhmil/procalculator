//
//  AppDelegate.h
//  ProCalculator
//
//  Created by Khmil Vasyl on 6/26/13.
//  Copyright (c) 2013 Khmil Vasyl. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProCalculatorViewController;
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) ProCalculatorViewController *proCalculatorViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
